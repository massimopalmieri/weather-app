import React, {Component} from 'react'

import {BaseUrl, params} from '../config'
import {groupByDay, getQueryString} from '../utils'
import Today from '../Today/index.js'
import TodaySegment from '../TodaySegment/index'
import FollowingDay from '../FollowingDay/index'
import {AppWrapper, Title, SubTitle, TodaySegments, FollowingDays} from './style'

const fullUrl = `${BaseUrl}?${getQueryString(params)}`

class App extends Component {
  constructor () {
    super()

    this.state = {
      data: null
    }
  }

  componentDidMount () {
    if (window && window.fetch) {
      window.fetch(fullUrl)
        .then(
          res => res.json(),
          error => console.log(error)
        )
        .then(data => this.setState({data}))
    }
  }

  render () {
    if (!this.state.data) {
      return <AppWrapper>Loading...</AppWrapper>
    }

    const {data: {city, list, list: [currentWeather]}} = this.state
    const groupedByDay = groupByDay(list)
    const [today, ...rest] = Object.keys(groupedByDay)

    return (
      <AppWrapper>
        <Title>The weather in {city.name} is:</Title>
        <Today prediction={currentWeather}/>

        <SubTitle>Rest of the day:</SubTitle>
        <TodaySegments>
          {groupedByDay[today].map(prediction => (
            <TodaySegment key={prediction.dt} prediction={prediction}/>
          ))}
        </TodaySegments>

        <FollowingDays>
          <h2>Next {rest.length} days:</h2>
          {rest.map(date => (
            <FollowingDay key={date} date={date} segments={groupedByDay[date]}/>
          ))}
        </FollowingDays>
      </AppWrapper>
    )
  }
}

export default App
