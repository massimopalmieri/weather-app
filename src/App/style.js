import styled from 'styled-components'

export const AppWrapper = styled.div`
  max-width: 800px;
  margin: 20px auto;
  padding: 20px;
`

export const Title = styled.h1`
  margin-bottom: 30px;
  font-size: 24px;
`

export const SubTitle = styled.h2`
  font-size: 18px;
  margin-top: 40px;
`

export const TodaySegments = styled.div`
  align-items: center;
  border-top: 2px dashed #dedede;
  display: flex;
  flex-direction: column;
  margin-top: 30px;
  padding-top: 20px;
  
  @media (min-width: 600px) {
    flex-direction: row;
  }
`

export const FollowingDays = styled.div`
  margin-top: 80px;
`
