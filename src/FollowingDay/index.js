import React, {Component} from 'react'
import moment from 'moment'

import {format, getMode} from '../utils'
import {Wrapper, Date, Icon, Temperature, Description} from './style'

class FollowingDay extends Component {
  render () {
    const {segments, date} = this.props
    const min = Math.min(...segments.map(segments => Math.round(segments.main.temp_min)))
    const max = Math.max(...segments.map(segments => Math.round(segments.main.temp_max)))
    const midDay = segments[Math.floor(segments.length / 2)]
    const {weather: [weather]} = midDay
    const icons = segments.map(segment => segment.weather[0].id)
    const icon = getMode(icons)

    return (
      <Wrapper>
        <Date>{moment(date, format).format('dddd, MMMM Do')}</Date>
        <Icon iconId={icon}/>
        <Temperature>{max}° / {min}°</Temperature>
        <Description>{weather.description}</Description>
      </Wrapper>
    )
  }
}

export default FollowingDay
