import styled from 'styled-components'

export const Wrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  margin-top: 20px;
  padding-top: 20px;
  border-top: 2px dashed #dedede;
`

export const Date = styled.div`
  flex: 2;
`

export const Icon = styled.i.attrs({
  className: props => `wi wi-fw wi-owm-${props.iconId}`
})`
  font-size: 30px;
  flex: 2;
`

export const Temperature = styled.div`
  font-size: 18px;
  flex: 2;
`

export const Description = styled.div`
  flex: 2;
`
