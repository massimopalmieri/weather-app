import React, {Component} from 'react'

import {getCardinalDirection} from '../utils'
import {Wrapper, Content, Icon, Temperature, Description, Info} from './style'

class Today extends Component {
  render () {
    const {prediction} = this.props
    const {main, weather: [weather], wind} = prediction

    return (
      <Wrapper>
        <Icon iconId={weather.id}/>
        <Content>
          <Temperature>{Math.round(main.temp_max)}°</Temperature>
          <Description>{weather.description}</Description>
          <Info>Humidity: {main.humidity}%</Info>
          <Info>Wind: {getCardinalDirection(wind.deg)}</Info>
        </Content>
      </Wrapper>
    )
  }
}

export default Today
