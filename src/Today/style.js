import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const Content = styled.div`
  display: flex;
  flex-direction: column;
`

export const Icon = styled.i.attrs({
  className: props => `wi wi-fw wi-owm-${props.iconId}`
})`
  font-size: 140px;
  margin: 0 20px;
`

export const Temperature = styled.div`
  font-size: 72px;
`

export const Description = styled.div`
  display: flex;
  font-size: 50px;
`

export const Info = styled.div`
  font-size: 16px;
  margin-top: 10px;
  color: #555;
`
