import React, {Component} from 'react'
import moment from 'moment'

import {getCardinalDirection} from '../utils'
import {Wrapper, Item, Content, Icon, Temperature, Description, Info, Time} from './style'

class TodayHourlySegment extends Component {
  render () {
    const {prediction} = this.props
    const {main, weather: [weather], wind} = prediction

    return (
      <Item>
        <Wrapper>
          <Icon iconId={weather.id}/>
          <Content>
            <Temperature>{Math.round(main.temp_max)}°</Temperature>
            <Description>{weather.description}</Description>
            <Info>Humidity: {main.humidity}%</Info>
            <Info>Wind: {getCardinalDirection(wind.deg)}</Info>
          </Content>
        </Wrapper>
        <Time>{moment.unix(prediction.dt).format('h:mma')}</Time>
      </Item>
    )
  }
}

export default TodayHourlySegment
