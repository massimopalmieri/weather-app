import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const Item = styled.div`
  flex: 1;
  width: 100%;
`

export const Content = styled.div`
  display: flex;
  flex-direction: column;
`

export const Icon = styled.i.attrs({
  className: props => `wi wi-fw wi-owm-${props.iconId}`
})`
  font-size: 30px;
  margin: 0 20px;
`

export const Temperature = styled.div`
  font-size: 20px;
`

export const Description = styled.div`
  display: flex;
  font-size: 16px; 
`

export const Info = styled.div`
  font-size: 12px;
  margin-top: 5px;
  color: #555;
`

export const Time = styled.div`
  text-align: center;
  padding-top: 10px;
  border-top: 2px dashed #dedede;
  margin: 20px 5px 0;
  font-size: 14px;
  letter-spacing: 2px;
  font-size: 12px;
  text-transform: uppercase;
  color: #999;
`
