import React from 'react'
import ReactDOM from 'react-dom'
import 'weather-icons/css/weather-icons.css'

import './index.css'
import App from './App/index.js'

ReactDOM.render(<App />, document.getElementById('root'))
