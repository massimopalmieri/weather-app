import moment from 'moment'

export const format = 'DD-MM-YYYY'

export const groupByDay = list => list.reduce((accumulator, currentValue) => {
  const date = moment.unix(currentValue.dt).format(format)
  if (!accumulator[date]) {
    accumulator[date] = []
  }
  accumulator[date].push(currentValue)
  return accumulator
}, {})

// from https://gist.github.com/basarat/4670200
export function getCardinalDirection (angle) {
  if (typeof angle === 'string') {
    angle = parseInt(angle, 10)
  }

  if (angle <= 0 || angle > 360 || typeof angle === 'undefined') {
    return '☈'
  }

  const arrows = {
    north: '↑ N',
    north_east: '↗ NE',
    east: '→ E',
    south_east: '↘ SE',
    south: '↓ S',
    south_west: '↙ SW',
    west: '← W',
    north_west: '↖ NW'
  }
  const directions = Object.keys(arrows)
  const degree = 360 / directions.length

  angle = angle + degree / 2

  for (let i = 0; i < directions.length; i++) {
    if (angle >= (i * degree) && angle < (i + 1) * degree) {
      return arrows[directions[i]]
    }
  }

  return arrows['north']
}

export const getMode = array => {
  return array.sort((a, b) => {
    return array.filter(value => value === a).length - array.filter(value => value === b).length
  }).pop()
}

export const getQueryString = params => {
  return Object.keys(params).map(
    key => `${key}=${params[key]}`
  ).join('&')
}
