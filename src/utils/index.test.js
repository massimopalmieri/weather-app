import moment from 'moment'

import * as utils from './utils'

it('should return the item with the highest occurrence', () => {
  const array = [1, 2, 3, 4, 5, 2, 2, 1]
  expect(utils.getMode(array)).toBe(2)
})

it('should group segments by date', () => {
  const dates = ['12-06-2017', '13-06-2017', '14-06-2017']
  const unixDates = [
    moment(dates[0], utils.format).unix(),
    moment(dates[1], utils.format).unix(),
    moment(dates[2], utils.format).unix()
  ]
  const segments = [
    {dt: unixDates[0], temp: 10},
    {dt: unixDates[0], temp: 12},
    {dt: unixDates[1], temp: 11},
    {dt: unixDates[1], temp: 10},
    {dt: unixDates[2], temp: 11}
  ]
  const groupedSegments = utils.groupByDay(segments)

  expect(groupedSegments[dates[0]].length).toBe(2)
  expect(groupedSegments[dates[1]].length).toBe(2)
  expect(groupedSegments[dates[2]].length).toBe(1)
})

it('should return the cardinal direction', () => {
  expect(utils.getCardinalDirection(1)).toBe('↑ N')
  expect(utils.getCardinalDirection(45)).toBe('↗ NE')
  expect(utils.getCardinalDirection(91)).toBe('→ E')
  expect(utils.getCardinalDirection(140)).toBe('↘ SE')
  expect(utils.getCardinalDirection(180)).toBe('↓ S')
  expect(utils.getCardinalDirection(230)).toBe('↙ SW')
  expect(utils.getCardinalDirection(271)).toBe('← W')
  expect(utils.getCardinalDirection(310)).toBe('↖ NW')
  expect(utils.getCardinalDirection(360)).toBe('↑ N')
})

it('should ', () => {
  const params = {
    q: 'query',
    apiKey: '12345'
  }

  expect(utils.getQueryString(params)).toBe(`q=${params.q}&apiKey=${params.apiKey}`)
})
